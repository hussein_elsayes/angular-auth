import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class JwtGuard implements CanActivate {
  
  constructor(private _router : Router){}

  canActivate():boolean {
    if(this.hasToken())
    {
      return true;
    }else
    {
      this._router.navigate(['login']);
      return false;
    }
  }

  hasToken()
  {
    return !!localStorage.getItem('token');
  }
}
