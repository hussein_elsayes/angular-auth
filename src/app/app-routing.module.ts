import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HomeComponent } from './components/home/home.component';
import { JwtGuard } from './guards/jwt.guard';
const routes: Routes = [
  {path : 'login', component: LoginComponent},
  {
    path : 'home',
    component : HomeComponent,
    canActivate : [JwtGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
    BrowserModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
