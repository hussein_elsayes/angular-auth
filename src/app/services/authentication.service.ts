import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { LoginModel } from '../models/login-model';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
_baseUrl = 'http://localhost:8095/api/signin';

  constructor(private _httpClient : HttpClient) { }

  login(_loginModel: LoginModel)
  {
    return this._httpClient.post<any>(this._baseUrl,_loginModel);
  }
}
