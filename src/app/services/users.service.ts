import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { User } from '../models/user';
import {catchError} from 'rxjs/operators/';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private _httpClient : HttpClient) { }

  private _url = "http://localhost:9879/api/users/";

  getAllUsers()
  {
      return this._httpClient.get<User>(this._url+ "getAll");
  }

  addUser(user :User)
  {
      return this._httpClient.post(this._url+ "add",user).pipe(catchError(this.errorHandler));
  }
  
  testError(user :User)
  {
      return this._httpClient.post(this._url+ "error",user).pipe(catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse)
  {
    return throwError(error);
  }
}
