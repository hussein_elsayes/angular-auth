import { Injectable } from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest, HttpEvent} from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req : HttpRequest<any> , next : HttpHandler) : Observable<HttpEvent<any>>
  {
    let tokenizedRequest = req.clone(
      {
        setHeaders : {'Authorization' : 'Bearer ' + this.getToken()}
      }
    );
    return next.handle(tokenizedRequest);
  }

  getToken()
  {
    console.log(localStorage.getItem('token'));
    return localStorage.getItem('token');
  }
}
