import { Injectable } from '@angular/core';
import {HttpInterceptor,HttpRequest,HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http'
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService implements HttpInterceptor {

  constructor(private _router :Router) { }

  intercept( request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>>
  {
  return next.handle(request).pipe
    (
      catchError(err => {
            if(err.status === 401 )
            {
              localStorage.removeItem('token');
              this._router.navigate(['login']);
            }          
            let error = err.error.message || err.statusText;
            return throwError(error);
          }
        )
    )
  }
}
