import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _userService : UsersService) { }

users;

  ngOnInit() {
    this._userService.getAllUsers().subscribe(data => this.users =  data);
  }

}
