import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../../models/login-model';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _authenticationService : AuthenticationService, private _router:Router) { }

  userModel = new LoginModel('hussein','Matrix_501');
  _token;
  
  ngOnInit() {
  }

  onSubmit()
  {
    this._authenticationService.login(this.userModel)
    .subscribe(data =>
      {
        localStorage.setItem('token',data.token);
        this._router.navigate(['/home']);
      });  
  }

}
